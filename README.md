# HFA URL Shortener

Description: 
  - Create a demo application allowing to shorten url
  - Details in the PDF (The wireframe are very basic but I'll let you use your inside designer voice make the good choice to make your work prettier)

## Objective: 
  Understand if you are a good choice for the fullstack position (strengths and weaknesses on backend, frontend and communication skills)

## Steps:
  - Step 1: Base
    - Homepage display an input allowing to paste an URL and a shorten button.
    - When a user put a URL into the field and click on the shorten button it will generate a shorten URL valid for a limited amount of time (by default 3 days)
    - The user will be prompt to copy the URL and after clicking it will be added to his clipboard
    - If the user doesn't want the automatic copy he should be able to copy the text manually.

  - Step 2: Connected interface
    - User can connect to an interface by using it's login and password
    - User access an interface where a list of all the shorten url are appearing (details in PDF)
    - When clicking on a row the user access to the statistics of it's shorten URL usage:
      - Bar chart whose abscissa is displaying the number of days the link has been created (in days) and ordinate the number of clicks per days
    - User has also access to the URL shortener to create new URLs and set manually their expiration date (from 1 to 30 days)
    - User can decide to manually expire a URL or to extend it's duration.

  - Step 3: What can we do to improve our service?
    - Contribute to the brainstorming by giving some ideas (with a little bit of details to have enough to start a discussion)
    - You can even do a POC/wireframe/etc if you want.
  
  - Bonus Step - not mandatory: Stats for unregistered users
    - New idea from the product team, allow the unregistered user to have access to the stats of their only available shorten URL, so:
      - When an unregistered user access to the homepage it is automatically identified that he has already used the service and he has the choice to view the stats of his previous usage of the service.

## Tips:
  - Chart library: https://www.chartjs.org/
  - Tests: Rspec, Jest

## Requirements:
  - Limit as much as possible the reload of the page (we are in a dynamic world why should my page should need be reloaded!)
  - All the page are responsive and visible on an Iphone, tablet or a desktop
  - Even if the interface display duration in days it is, for the test, express in minute, the maximum duration of a link is of 30 minutes for a registered account and 3 for an unregistered one.
  - By default only one account and you should provide the login/password to test the authenticated part
  - Stats for registered user are automatically generated based on the given duration (2 differents URLs should not have the same stats or it's a total misfortune).
  - Registered user can't create the same URL twice (except if the URL has been expired), if they tried they should be propose to use the valid one and maybe to extend it's live.
  - Details about your approach and comments will be much appreciated.
  - Test code should be available through Gitlab or Github and deployed on heroku for live testing (don't forget to give the URL)
  - Indicate in your README the amount of time you have passed on the exercice (It will help us to re-ajust it).

## Constraints:
    - For Mid & Senior Dev:
        - Usage of UJS if forbidden.
        - Usage of css frameworks like Bootstrap are not recommanded but if you are more backend than frontend it will be understandable.
        - Code will need to be fully tested, following the test pyramid logic (https://martinfowler.com/articles/practical-test-pyramid.html)
    
    - For Junior:
        - Usage of UJS is possible.
        - Usage of frameworks like Bootstrap is possible.
        - Code will need to be tested as much as possible, following the test pyramid logic (https://martinfowler.com/articles/practical-test-pyramid.html)
    
